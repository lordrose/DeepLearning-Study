# Overview
| Component                           | Environment     | Integrated Development Environment | Python |
|-------------------------------------|-----------------|------------------------------------|--------|
| 01_Local_Tensorflow_Official_Docker | Local Container | Jupyter Notebook                   | 2.7    |
| 02_Local_PyCharm_Anaconda           | Local macOS     | IntelliJ PyCharm                   | 3.5    |

# 01_Local_Tensorflow_Official_Docker
| Contents | Details                                                                                                                                                                                    |
|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Install  | [by docker and jupyter](01_Local_Tensorflow_Official_Docker/01_Install/01_by_Docker.md)                                                                                                    |
| Example  | [1st : Multi Variable Linear Regression with manually](01_Local_Tensorflow_Official_Docker/02_Workspace/01_1st_Example/01_Multi_Variable_Linear_Regression_with_manually.ipynb)            |
|          | [2nd : Multi Variable Linear Regression with Matrix](01_Local_Tensorflow_Official_Docker/02_Workspace/02_2nd_Example/02_Multi_Variable_Linear_Regression_with_Matrix.ipynb)                |
|          | [3rd : Multi Variable Linear Regression with file](01_Local_Tensorflow_Official_Docker/02_Workspace/03_3rd_Example/03_Multi_Variable_Linear_Regression_with_file.ipynb)                    |
|          | [4th : Multi Variable Linear Regression with fileQueue](01_Local_Tensorflow_Official_Docker/02_Workspace/04_4th_Example/04_Multi_Variable_Linear_Regression_with_fileQueue.ipynb)          |
|          | [5th : Multi Variable Linear Regression with Hattrick KP](01_Local_Tensorflow_Official_Docker/02_Workspace/05_5th_Example/05_Multi_Variable_Linear_Regression_with_file_Hattrick_KP.ipynb) |

# 02_SNU_DeepLearning_ReInforcement_Learning
| Contents | Details                                                                                                                                   |
|----------|-------------------------------------------------------------------------------------------------------------------------------------------|
| Install  | [Install PyCharm](02_SNU_ReInforcement_Learning/01_Install/01_Install_PyCharm.md)                                                         |
|          | [Create VirEnv](02_SNU_ReInforcement_Learning/01_Install/02_Create_VirEnv.md)                                                             |
|          | [Check Version of Packages](02_SNU_ReInforcement_Learning/01_Install/03_CheckVersionOfPackage.py)                                         |
| Book     | [Agent](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/01_Agent.md)                                               |
|          | [State](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/02_State.md)                                               |
|          | [Actions](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/03_Actions.md)                                           |
|          | [Reward](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/04_Reward.md)                                             |
|          | [Time](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/05_Time.md)                                                 |
|          | [Policy](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/06_Policy.md)                                             |
|          | [Reward in MDP](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/07_Reward_in_MDP.md)                               |
|          | [Flow in MDP](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/08_Flow_in_MDP.png)                                  |
|          | [State Transition Probability](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/09_State_Transition_Probability.md) |
|          | [Discount Factor](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/10_Discount_Factor.md)                           |
|          | [Return](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/11_Return.md)                                             |
|          | [ValueFunction](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/12_ValueFunction.md)                               |
|          | [QFunction](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/02_ch02/01_MDP/13_QFunction.md)                                       |
Reinforcement Learning by Python and Keras  
╠═2. MDP and BE  
╚═3. Grid World and Dynamic Programming  
&ensp; &ensp; ╠═3.1. [Policy Iteration](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/03_ch03/01_grid_world/01_policy_iteration/policy_iteration.py)  
&ensp; &ensp; ╚═3.2. [Value Iteration](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/03_ch03/01_grid_world/02_value_iteration/value_iteration.py)  
( ║ ╠ ═ ╚ )  



# 03_InSpace_Keras_Tutorial
| Contents                     | Details                                                                                                                                        |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| part 01 Start Keras          | [Install by docker](03_InSpace_Keras_Tutorial/01_Install_Keras/01_by_docker.md)                                                                |
|                              | [Check Version of Packages](03_InSpace_Keras_Tutorial/01_Install_Keras/02_Check_Library_Version.ipynb)                                         |
| part 02 DeepLearning Concept | [ch1 Train Validation DataSet](03_InSpace_Keras_Tutorial/02/01/01_Train_Validate_Test_Set.ipynb)                                               |
|                              | [ch2 BatchSize and Epochs](03_InSpace_Keras_Tutorial/02/02/01_batch_size_epochs.ipynb)                                                         |
|                              | [ch3 history by fit](03_InSpace_Keras_Tutorial/02/03/01_history_by_fit.ipynb)                                                                  |
|                              | [ch3 history by tensorboard](03_InSpace_Keras_Tutorial/02/03/02_history_by_tensorboard.ipynb)                                                  |
|                              | [ch3 history bu user defined callbacks](03_InSpace_Keras_Tutorial/02/03/03_history_by_user_defined_callbacks.ipynb)                            |
|                              | [ch4 OverFitting](03_InSpace_Keras_Tutorial/02/04/01_overfitting.ipynb)                                                                        |
|                              | [ch4 EarlyStopping](03_InSpace_Keras_Tutorial/02/04/02_early_stopping.ipynb)                                                                   |
|                              | [ch4 EarlyStopping with patience=10](03_InSpace_Keras_Tutorial/02/04/03_early_stopping_patience.ipynb)                                         |
|                              | [ch5 Evaluation for Model of Classfication](03_InSpace_Keras_Tutorial/02/05/01_eval_for_classification.ipynb)                                  |
|                              | [ch5 Evaluation for Model of Search or Detection](03_InSpace_Keras_Tutorial/02/05/02_eval_for_search.ipynb)                                    |
|                              | [ch5 Evaluation for Model of Split](03_InSpace_Keras_Tutorial/02/05/03_eval_for_split.ipynb)                                                   |
|                              | [ch6 Model view, save, load](03_InSpace_Keras_Tutorial/02/06/01_MNIST.ipynb)                                                                   |
| part 03 Layer Concept        | [ch01 Multi Perceptron Layer](03_InSpace_Keras_Tutorial/03/01/01_Neuron_and_Perceptron.ipynb)                                                  |
|                              | [ch02 Multi Perceptron Layer for Pigma Indians Diabetes](03_InSpace_Keras_Tutorial/03/02/01_perceptron-model-for-pigma-indians-diabetes.ipynb) |
|                              | [ch03 Convolution Layer](03_InSpace_Keras_Tutorial/03/03/01_Convolution_Layer.ipynb)                                                           |
|                              | [ch03 MaxPooling Layer](03_InSpace_Keras_Tutorial/03/03/02_MaxPooling_Layer.ipynb)                                                             |
|                              | [ch03 Flatten Layer](03_InSpace_Keras_Tutorial/03/03/03_Flatten_Layer.ipynb)                                                                   |
|                              | [ch03 Conv2D MaxPooling2D Flatten Example](03_InSpace_Keras_Tutorial/03/03/04_Conv2D_MaxPooling2D_Flatten_Example.ipynb)                       |
|                              | [ch03 icrawler on Google Image](03_InSpace_Keras_Tutorial/03/03/05_icrawler_on_Google_Image.ipynb)                                             |
|                              | [ch03 Duplicate Data by ImageDataGenerator](03_InSpace_Keras_Tutorial/03/03/06_Duplicate_with_ImageDataGenerator.ipynb)                        |

# 04_DeepLearning_from_Scratch  
&ensp;╠═ch1. [Check Python Version](04_DeepLearning_from_Scratch/1/1.3/python_version.ipynb)  
&ensp;╠═ch2. [Perceptron : Definition, example for AND, NAND, OR, XOR](04_DeepLearning_from_Scratch/2/Perceptron.ipynb)  
&ensp;╚═ch3. [From Perceptron to ANN : Step Function, Sigmoid Function](04_DeepLearning_from_Scratch/3/3.1/01_from_Perceptron_to_Artificial_Neural_Network.ipynb)  

( ║ ╠ ═ ╚ )  
